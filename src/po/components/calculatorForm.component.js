import BaseComponent from "./base.component.js";

export default class CalculatorFormComponent extends BaseComponent {
    constructor() {
        super("[name='ComputeEngineForm']");
    }
    /**
     * 
     * @param {"noOfInstances" | "series" | "machineType" | "gpuType" | "ssd" | "datacenterLocation" | "gpuCount" | "usage"} name 
     */
    input(name) {
        const selectors = {
            noOfInstances: "[ng-model='listingCtrl.computeServer.quantity']",
            series: "[ng-model='listingCtrl.computeServer.series']",
            machineType: "[ng-model='listingCtrl.computeServer.instance']",
            gpuType: "[ng-model='listingCtrl.computeServer.gpuType']",
            ssd: "[ng-model='listingCtrl.computeServer.ssd']",
            datacenterLocation: "[ng-model='listingCtrl.computeServer.location']",
            gpuCount: "[ng-model='listingCtrl.computeServer.gpuCount']",
            usage: "[ng-model='listingCtrl.computeServer.cud']"
        }
        return this.rootEl.$(selectors[name])
    }

    selectSeries(name) {
        const selectors = {
            n1: "[value='n1']",
            c3d: "[value='c3d']"
        }
        return $(selectors[name])
    }

    selectMachineType(name) {
        const selectors = {
            standard8: "[value='CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-8']",
            highcpu360: "[value='CP-COMPUTEENGINE-VMIMAGE-C3D-HIGHCPU-360']"
        }
        return $(selectors[name])
    }

    selectLocation(name) {
        const selectors = {
            frankfurt: "[ng-repeat='item in listingCtrl.fullRegionList | filter:listingCtrl.inputRegionText.computeServer'][value='europe-west3'] .md-text",
            netherlands: "[ng-repeat='item in listingCtrl.fullRegionList | filter:listingCtrl.inputRegionText.computeServer'][value='europe-west4'] .md-text"
        }
        return $(selectors[name])
    }

    selectGPUType(name) {
        const selectors = {
            t4: "[value='NVIDIA_TESLA_T4'] .md-text"
        }
        return $(selectors[name])
    }

    selectGPUNumber(name) {
        const selectors = {
            one: "[ng-repeat='item in listingCtrl.supportedGpuNumbers[listingCtrl.computeServer.gpuType]'][value='1'] .md-text"
        }
        return $(selectors[name])
    }

    selectSSD(name) {
        const selectors = {
            two: "[ng-repeat='item in listingCtrl.dynamicSsd.computeServer'][value='2']"
        }
        return $(selectors[name])
    }

    selectUsage(name) {
        const selectors = {
            oneYear: "#select_option_138 .md-text",
            threeYears: "#select_option_139 .md-text"
        }
        return $(selectors[name])
    }


    get addGPUs() {
        return $("[ng-model='listingCtrl.computeServer.addGPUs']");
    }

    get addToEstimateBtn() {
        return $("button[ng-click='listingCtrl.addComputeServer(ComputeEngineForm);']");
    }

    async setGPUTo(type, count) {
        await this.addGPUs.click();
        await this.input("gpuType").click();
        await this.selectGPUType(type).waitForDisplayed();
        await this.selectGPUType(type).click();
        await this.input("gpuCount").click();
        await this.selectGPUNumber(count).waitForDisplayed();
        await this.selectGPUNumber(count).click();
    }

    async setNoOfInstancesTo(number) {
        await this.input("noOfInstances").setValue(number);
    }

    async setSeriesTo(name) {
        await this.input("series").click();
        await this.selectSeries(name).click();
    }

    async setMachineTypeTo(name) {
        await this.input("machineType").click();
        await this.selectMachineType(name).waitForDisplayed();
        await this.selectMachineType(name).click();
    }

    async setSSDTo(name) {
        await this.input("ssd").click();
        await this.selectSSD(name).waitForDisplayed();
        await this.selectSSD(name).click();
    }

    async setLocationTo(name) {
        await this.input("datacenterLocation").click();
        await this.selectLocation(name).waitForDisplayed();
        await this.selectLocation(name).click();
    }

    async setUsageTo(name) {
        await this.input("usage").click();
        await this.selectUsage(name).waitForDisplayed();
        await this.selectUsage(name).click();

    }

    async addToEstimate() {
        await this.addToEstimateBtn.click();
    }

}