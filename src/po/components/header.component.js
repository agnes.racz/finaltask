import BaseComponent from "./base.component.js";

const ENTER = "\uE007";

export default class HeaderComponent extends BaseComponent {
    constructor() {
        super(".TDbJKc");
    }

    get searchBtn() {
        return this.rootEl.$("[aria-label='Search']");
    }

    async searchFor(text) {
        await this.searchBtn.click();
        await this.searchBtn.setValue(text);
        await browser.keys(ENTER);
    }

}