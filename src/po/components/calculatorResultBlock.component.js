import BaseComponent from "./base.component.js";

export default class CalculatorResultBlock extends BaseComponent {
    constructor() {
        super("#resultBlock");
    }

    /**
     * 
     * @param {"noOfInstances" | "datacenterLocation" | "usage" | "model" | "machineType" | "gpuType" | "ssd"} name 
     * @returns 
     */
    output(name) {
        const selectors = {
            noOfInstances: "//*[contains(text(),' x')]",
            datacenterLocation: "//*[contains(text(),'Region:')]",
            usage: "//*[contains(text(),'Commitment term:')]",
            model: "//*[contains(text(),'model:')]",
            machineType: "//*[contains(text(),'Instance type:')]",
            gpuType: "//*[contains(text(),'GPU dies:')]",
            ssd: "//*[contains(text(),'SSD:')]"
        }
        return this.rootEl.$(selectors[name])
    }

    get cartTotal() {
        return $(".cpc-cart-total");
    }

}