import BaseComponent from "./base.component.js";

export default class CalculatorHeaderComponent extends BaseComponent {
    constructor() {
        super(".main-content");
    }

    get computeEngineBtn() {
        return this.rootEl.$(".compute");
    }

    async selectComputeEngine() {
        await this.computeEngineBtn.click();
    }

}