import BasePage from "./base.page.js";
import { HeaderComponent } from "../components/index.js";

export default class HomePage extends BasePage {

    constructor() {
        super("https://cloud.google.com/?hl=en");
        this.header = new HeaderComponent();
    }

    async getPageTitle() {
        return browser.getTitle();
    }
}
