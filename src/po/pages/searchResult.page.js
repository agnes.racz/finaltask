
import BasePage from "./base.page.js"
import { HeaderComponent } from "../components/index.js";

export default class SearchResultPage extends BasePage {

    constructor() {
        super("https://cloud.google.com/s/results?q=Google%20Cloud%20Platform%20Pricing%20Calculator&text=Google%20Cloud%20Platform%20Pricing%20Calculator");
        this.header = new HeaderComponent();
    }

    get pricingCalculatorLink() {
        return $("[data-ctorig='https://cloud.google.com/products/calculator-legacy']")
    }

    async selectPricingCalculatorLink() {
        await this.pricingCalculatorLink.click()
    }
}
