import { CalculatorHeaderComponent, CalculatorFormComponent, CalculatorResultBlock } from "../components/index.js";

export default class PricingCalculatorPage {

    constructor() {
        this.calculatorHeader = new CalculatorHeaderComponent();
        this.calculatorForm = new CalculatorFormComponent();
        this.calculatorResultBlock = new CalculatorResultBlock();
    }

    open() {
        return browser.url("https://cloudpricingcalculator.appspot.com/");
    }
}