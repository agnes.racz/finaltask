import BasePage from "./base.page.js";
import HomePage from "./home.page.js";
import PricingCalculatorPage from "./pricingCalculator.page.js";
import SearchResultPage from "./searchResult.page.js";

/**
 * 
 * @param {"basepage" | "homepage" | "pricingCalculatorPage" | "searchResultPage"} name 
 * @returns {BasePage | HomePage | PricingCalculatorPage | SearchResultPage}
 */

function factory(name) {
    const items = {
        basepage: new BasePage(),
        homepage: new HomePage(),
        pricingCalculatorPage: new PricingCalculatorPage(),
        searchResultPage: new SearchResultPage()
    }
    return items[name];
}

export {
    BasePage,
    HomePage,
    PricingCalculatorPage,
    SearchResultPage,
    factory
};