import { HeaderComponent } from "../components/index.js";

export default class BasePage {

    constructor(url) {
        this.url = url;
        this.header = new HeaderComponent();
    }

    open() {
        return browser.url(this.url);
    }
}