import { factory } from "../po/pages/index.js";

describe('Smoke tests suite', () => {

    beforeEach(async () => {
        await factory("homepage").open();
    })

    it("Verify if the Google Cloud page loads successfully", async () => {
        await expect(browser).toHaveTitle("Cloud Computing Services | Google Cloud");

        const element = await factory("homepage").header.searchBtn;
        await expect(element).toBeDisplayed();
        
    });
})