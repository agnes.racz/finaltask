
import { factory } from "../po/pages/index.js";
import { cheapEngineData, cheapEngineExpected } from "../data/cheapEngineTest-data.js";
import { expensiveEngineData, expensiveEngineExpected } from "../data/expensiveEngineTest-data.js";

const TEXT = "Google Cloud Platform Pricing Calculator";

describe('Simple tests suite', () => {

    beforeEach(async () => {
        await factory("homepage").open();
    })

    it('Verify if the result block contains all input values for a cheap engine estimation', async () => {
        await factory("homepage").header.searchFor(TEXT);
        await factory("searchResultPage").selectPricingCalculatorLink();

        await factory("pricingCalculatorPage").open(); //had some trouble finding the selectors after opening the calculator, so used this as a workaround
        await factory("pricingCalculatorPage").calculatorHeader.selectComputeEngine();

        const inputForm = factory("pricingCalculatorPage").calculatorForm;
        await inputForm.setNoOfInstancesTo(cheapEngineData.noOfInstances);
        await inputForm.setSeriesTo(cheapEngineData.series);
        await inputForm.setMachineTypeTo(cheapEngineData.machineType);
        await inputForm.setGPUTo(cheapEngineData.gpuType, cheapEngineData.gpuCount);
        await inputForm.setSSDTo(cheapEngineData.ssd);
        await inputForm.setLocationTo(cheapEngineData.location);
        await inputForm.setUsageTo(cheapEngineData.usage);
        await inputForm.addToEstimate();

        const resultForm = factory("pricingCalculatorPage").calculatorResultBlock;
        await expect(resultForm.cartTotal.toHaveText(`Total Estimated Cost: USD ${expensiveEngineExpected.estimatedResult} per 1 month`));
        await expect(resultForm.output("noOfInstances")).toHaveTextContaining(cheapEngineExpected.noOfInstances);
        await expect(resultForm.output("datacenterLocation")).toHaveTextContaining(cheapEngineExpected.location);
        await expect(resultForm.output("usage")).toHaveTextContaining(cheapEngineExpected.usage);
        await expect(resultForm.output("model")).toHaveTextContaining(cheapEngineExpected.model);
        await expect(resultForm.output("machineType")).toHaveTextContaining(cheapEngineExpected.machineType);
        await expect(resultForm.output("gpuType")).toHaveTextContaining(cheapEngineExpected.gpuType);
        await expect(resultForm.output("ssd")).toHaveTextContaining(cheapEngineExpected.ssd);
    })

    it('Verify if the result block contains all input values for an expensive engine estimation', async () => {
        await factory("pricingCalculatorPage").open();
        await factory("pricingCalculatorPage").calculatorHeader.selectComputeEngine();
        const inputForm = factory("pricingCalculatorPage").calculatorForm;
        await inputForm.setNoOfInstancesTo(expensiveEngineData.noOfInstances);
        await inputForm.setSeriesTo(expensiveEngineData.series);
        await inputForm.setMachineTypeTo(expensiveEngineData.machineType);
        await inputForm.setLocationTo(expensiveEngineData.location);
        await inputForm.setUsageTo(expensiveEngineData.usage);
        await inputForm.addToEstimate();

        const resultForm = factory("pricingCalculatorPage").calculatorResultBlock;
        await expect(resultForm.cartTotal.toHaveText(`Total Estimated Cost: USD ${expensiveEngineExpected.estimatedResult} per 1 month`));
        await expect(resultForm.output("noOfInstances")).toHaveTextContaining(expensiveEngineExpected.noOfInstances);
        await expect(resultForm.output("datacenterLocation")).toHaveTextContaining(expensiveEngineExpected.location);
        await expect(resultForm.output("usage")).toHaveTextContaining(expensiveEngineExpected.usage);
        await expect(resultForm.output("model")).toHaveTextContaining(expensiveEngineExpected.model);
        await expect(resultForm.output("machineType")).toHaveTextContaining(expensiveEngineExpected.machineType);
    })
})