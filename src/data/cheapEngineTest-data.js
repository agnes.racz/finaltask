export const cheapEngineData = {
    noOfInstances: "4",
    series: "n1",
    machineType: "standard8",
    gpuType: "t4",
    gpuCount: "one",
    ssd: "two",
    location: "frankfurt",
    usage: "oneYear"
}

export const cheapEngineExpected = {
    noOfInstances: "4",
    location: "Frankfurt",
    usage: "1 Year",
    model: "Regular",
    machineType: "n1-standard-8",
    gpuType: "1 NVIDIA TESLA T4",
    ssd: "2x375",
    estimatedResult: "1,840.40"
}
