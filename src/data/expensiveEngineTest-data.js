export const expensiveEngineData = {
    noOfInstances: "10",
    series: "c3d",
    machineType: "highcpu360",
    location: "netherlands",
    usage: "threeYears",
}

export const expensiveEngineExpected = {
    noOfInstances: "10",
    location: "Netherlands",
    usage: "3 Years",
    model: "Regular",
    machineType: "c3d-highcpu-360",
    estimatedResult: "46,537.28"
}
